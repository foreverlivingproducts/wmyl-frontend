##Get started
Just run: `$ npm install`

##Build project
###Build actions
**In this version you will need two terminal windows to watch the project.**
 * `$ npm run serve` *Serve fractal and watch twig files*
 * `$ npm run watch` *Watch scripts*
 * `$ npm run build` *Build for production*

 
###Existing tasks 
**From Gulp** 

* `$ gulp`  *Watch for changes and serve project*
* `$ gulp sass` *Merge sass to css with autoprefixes*
* `$ gulp js` *Merge all js files to one*
* `$ gulp fractal:start` *Run and serve Fractal*
* `$ gulp fratal:build` *Build Fractal and place in build folder*
* `$ gulp watch` *Watch fields and rebuild project*

**From NPM** 

* `$ npm run watch` *Watch changes in src*
* `$ npm run serve` *Serve fractal*


##Folder Structure
* **docs** - *Documentation for the project*
* **public** - *Builded project*
* **src** - *Font end assets like; sass, js and other assets*
* **static** - *Builded files from 'src-folder'*
* **template** - *twig templates for atoms, molecules, organisms and page templates*
