module.exports = function (gulp) {
	const path = require('path');
	const fractal = require('@frctl/fractal').create();

	/*
	 * Fractal settings
	 */
	fractal.set('project.title', 'FLP - front end library');
	fractal.components.set('path', path.join(__dirname, 'templates'));
	fractal.docs.set('path', path.join(__dirname, 'docs'));
	fractal.web.set('static.path', path.join(__dirname, 'static'));
	fractal.web.set('builder.dest', __dirname + '/public');

	fractal.web.set('server.syncOptions', {
		open: true,
		notify: true
	});

	/*
	 * Fractal Theme settings
	 */
	const mandelbrot = require('@frctl/mandelbrot');
	const customisedTheme = mandelbrot({
		skin: "black"
	});

	fractal.web.theme(customisedTheme);

	const logger = fractal.cli.console; // keep a reference to the fractal CLI console utility

	/*
	 * Start the Fractal server
	 */
	gulp.task('fractal:start', function(){
		const server = fractal.web.server({
			sync: true,
		});

		server.on('error', err => logger.error(err.message));
		return server.start().then(() => {
			logger.success(`Fractal server is now running at ${server.url}`);
		});
	});

	/*
	 * Run a static export of the project web UI.
	 *
	 * The build destination will be the directory specified in the 'builder.dest'
	 * configuration option set above.
	 */
	gulp.task('fractal:build', function(done){
		const builder = fractal.web.builder();
		builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
		builder.on('error', err => logger.error(err.message));
		return builder.build().then(() => {
			logger.success('Fractal build completed!');
			done();
		});
	});
};
