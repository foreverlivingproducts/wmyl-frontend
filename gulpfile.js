// 'use strict';
let paths = {
	styles: {
		src: './src/scss/**/*.scss',            		//Source files
		dest: './static/styles',               			//Public destination to folder
		pubDest: './static/styles/**/*.css',    		//Public destination to files in folder
		styleguide: './src/scss/styleguide/**/*.scss'	// Path to styleguide specific scss
	},
	scripts: {
		src: './src/js/**/*.js',                //Source files
		dest: './static/js/',                   //Public destination to folder
		pubDest: './static/js/**/*.js'          //Public destination to files in folder
	},
	fonts: {
		src: './src/fonts/**/*',
		dest: './static/fonts/',
	},
	images: {
		src: './src/images/**/*',
		dest: './static/images/',
	},
	symbol: {
		src: './src/symbol/**/*',
		dest: './static/symbol/',
	}
};

//general
const gulp = require('gulp');
const browserSync = require('browser-sync');
const del = require('del');
const concat = require('gulp-concat');
const gutil = require('gulp-util');
const notify = require('gulp-notify');
const buffer = require('vinyl-buffer');

// styles
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');

// js
const source = require('vinyl-source-stream');
let uglify = require('gulp-uglify');
const browserify = require('browserify');
const babelify = require('babelify');

// svg
const svgSprite = require('gulp-svg-sprite');
require('./fractal.js')(gulp);

/*
*  Error handling
*/
const handleError = function (task, err) {
	gutil.beep();

	notify.onError({
		message: task + ' failed, check the logs..',
		sound: false
	})(err);

	gutil.log(gutil.colors.bgRed(task + ' error:'), gutil.colors.red(err));
};

/*
* Clean build files
*/
gulp.task('del', function (done) {
	del(['./public/*', './static/*']);
	done();
});

gulp.task('clean', gulp.series('del', function (done) {
	done();
}));

/*
*  Pack Style and Scrips
*/
gulp.task('sass', function (done) {
	gulp.src(paths.styles.src)
		.pipe(sass({includePaths: ['scss']}))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(paths.styles.dest));
	done();
});

gulp.task('pack-styles', function () {
	return gulp.src([
		paths.styles.src,
		'!./src/scss/styleguide/**/*.scss' // Exclude styleguide
		])
		.pipe(sass({includePaths: ['scss']}))
		.pipe(autoprefixer({
			browsers: ['last 3 versions'],
			cascade: false
		}))
		.pipe(concat('main.css'))
		.pipe(cleanCss())
		.pipe(gulp.dest(paths.styles.dest));
});

/* Pack styleguide */
gulp.task('pack-styleguide-styles', function () {
	return gulp.src(paths.styles.styleguide)
		.pipe(sass({includePaths: ['scss']}))
		.pipe(autoprefixer({
			browsers: ['last 3 versions'],
			cascade: false
		}))
		.pipe(concat('styleguide.css'))
		.pipe(cleanCss())
		.pipe(gulp.dest(paths.styles.dest));
});

gulp.task('js', function () {
	return browserify({entries: './src/js/app.js', debug: true})
		.transform('babelify', {
			presets: ['es2015'],
			plugins: ["transform-object-assign", ['transform-es2015-classes', {loose: true}]]
		})
		.bundle()
		.on('error', function (err) {
			handleError('Browserify', err);
			this.emit('end');
		})
		.pipe(source('app.js'))
		.pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('pack-js', function () {
	return browserify({entries: './src/js/app.js', debug: true})
		.transform('babelify', {
			presets: ['es2015'],
			plugins: ["transform-object-assign", ['transform-es2015-classes', {loose: true}]]
		})
		.bundle()
		.on('error', function (err) {
			handleError('Browserify', err);
			this.emit('end');
		})
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(gulp.dest(paths.scripts.dest));
});

/*
* svg to symbol sprite
*/
gulp.task('svg', function () {
	return gulp.src('./src/icons/*.svg')
		.pipe(svgSprite({
			mode: {
				symbol: true
			}
		}))
		.on('error', function (err) {
			handleError('Browserify', err);
			this.emit('end');
		})
		.pipe(gulp.dest('./src/'))
		.pipe(gulp.dest('./static/'));
});

/*
* Static files
*/
gulp.task('data', function () {
	return gulp.src('./src/js/data/*')
		.pipe(gulp.dest('./static/data/'));
});

gulp.task('images', function () {
	return gulp.src(paths.images.src)
		.pipe(gulp.dest(paths.images.dest));
});

gulp.task('fonts', function () {
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(paths.fonts.dest));
});

/*
* Build tasks
*/
gulp.task('browser-sync', function (done) {
	browserSync.init([paths.styles.pubDest, paths.scripts.pubDest], {
		server: {
			baseDir: "./public/",
		},
		notify: true,
		open: false
	});
	done();
});

/*
* Default:  Watching for changes and rebuilds Fractal.
*/
gulp.task('watch', gulp.parallel('sass', 'js', 'browser-sync', function (done) {
	let watcher = gulp.watch([paths.styles.src, paths.scripts.src]);
	let htmlWatcher = gulp.watch(['./templates/**/*.hbs', './templates/**/*.config.json']);

	watcher.on('all', gulp.series('sass', 'js', 'fractal:build', function (done) {
		done();
	}));

	htmlWatcher.on('all', gulp.series('fractal:build', function (done) {
		browserSync.reload();
		done();
	}));

	done();
}));

/*
* Watch script files.
*/
gulp.task('assets-watch', gulp.parallel('sass', 'js', 'svg', 'images', 'fonts', 'browser-sync', function (done) {
	let styleWatcher = gulp.watch([paths.styles.src]);
	let scriptWatcher = gulp.watch([paths.scripts.src]);
	let imageWatcher = gulp.watch([paths.images.src]);
	let iconWatcher = gulp.watch(['./src/icons']);
	let fontWatcher = gulp.watch([paths.fonts.src]);

	styleWatcher.on('all', gulp.series('sass', function (done) {
		console.log('change in style');
		done();
	}));

	scriptWatcher.on('all', gulp.series('js', function (done) {
		console.log('change in script');
		done();
	}));

	imageWatcher.on('all', gulp.series('images', function (done) {
		console.log('change in image');
		done();
	}));

	iconWatcher.on('all', gulp.series('svg', function (done) {
		console.log('change in icon');
		done();
	}));

	fontWatcher.on('all', gulp.series('fonts', function (done) {
		console.log('change in fonts');
		done();
	}));

	done();
}));

/*
* Build and minify project
*/
gulp.task('build', gulp.series('svg', 'images', 'fonts', 'pack-styles', 'pack-styleguide-styles', 'pack-js', 'data', 'fractal:build', function (done) {
	console.log('Minify and build project');
	done();
}));
