# Image Assets

If you are using images (jpgs, pngs, gifs, svgs, etc.), this is the place to put them.

If you don't plan using images, feel free to delete this folder.

## Tasks and Files
