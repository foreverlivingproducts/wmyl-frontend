class toggleHeaderContent {
	constructor() {
		this.targetSelector = document.querySelectorAll('.js-toggle-menu-content');
		this.targetSelector = [].slice.call(this.targetSelector);

		if (this.targetSelector.length >= 1) {
			this.getElements(this.targetSelector);
		}
	}

	/* Get matching elements on the page */
	getElements(targets) {
		targets.forEach((element) => {
			let button = element;
			let targetSelector = element.getAttribute('aria-controls');
			let target = document.getElementById(targetSelector);

			this.toggleElements(button, target);
		});
	}

	/* Show selected content and hide the other */
	toggleElements (button, target) {
		button.addEventListener('click', ()=> {
			let parent = document.querySelector('.o-header');

			let visibleElements = parent.querySelectorAll('.js-is-visible');
			visibleElements = [].slice.call(visibleElements);

			let activeElements = parent.querySelectorAll('.js-is-active');
			activeElements = [].slice.call(activeElements);

			if (!target) {
				return
			}

			/* Hide all open elements */
			if (visibleElements.length > 0) {
				visibleElements.forEach((el) => {
					el.classList.remove('js-is-visible');
				});
			}

			/* Open targeted elements */
			if( !button.classList.contains('js-is-active') ) {
				activeElements.forEach((el) => {
					el.classList.remove('js-is-active');
				});

				target.classList.add('js-is-visible');
				button.classList.add('js-is-active');
			} else {
				target.classList.remove('js-is-visible');
				button.classList.remove('js-is-active');
			}
		});
	}
}

module.exports = toggleHeaderContent;
