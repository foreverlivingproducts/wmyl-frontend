class video {
	constructor() {
		this.videoAtoms = document.querySelectorAll('.m-video');
		this.videoAtoms = [].slice.call(this.videoAtoms);

		if(this.videoAtoms.length >= 1) {
			this.getVideos();
		}
	}

	/*
	* Get all video element on page
	*/
	getVideos() {
		this.ytVideos = [];
		this.vimeoVideos = [];

		this.videoAtoms.forEach((video, index)=> {
			let poster = video.querySelector('.m-video__poster');
			let type = video.getAttribute('data-video-type');

			let iframe = video.querySelector('.m-video__iframe');
			let id = 'video-'+index;
			iframe.setAttribute("id", id);

			if(type === 'youtube'){
				this.ytVideos.push({
					id: video.getAttribute('data-video-id'),
					element: video,
					elementId: id,
					poster: poster,
					player: null
				});
			} else if (type === 'vimeo') {
				this.vimeoVideos.push({
					id: video.getAttribute('data-video-id'),
					element: video,
					elementId: id,
					poster: poster,
					player: null
				});
			}
		});

		if(this.ytVideos.length > 0 ) {
			this._setYoutube();
		}
		if(this.vimeoVideos.length > 0) {
			this._loadVimeoScript();
		}
	}

	_setYoutube() {
		this._loadYTScript(() => {
			this.ytVideos.forEach((video) => {
				video.player = this._createYT(video);
			});
			this.eventListeners('youtube');
		});
	}

	/*
	*	Add YouTube Api as a script tag in header.
	*/
	_loadYTScript(scriptLoaded) {
		if (typeof YT !== 'undefined') scriptLoaded();

		let tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		let firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		window.onYouTubePlayerAPIReady = scriptLoaded;
	}

	/*
	*	Add Vimeo Api as a script tag in header.
	*	and run _createVimeo when loaded.
	*/
	_loadVimeoScript(scriptLoaded) {
		if (typeof Vimeo !== 'undefined') scriptLoaded();
		let tag = document.createElement('script');
		tag.src = "https://player.vimeo.com/api/player.js";
		let firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		tag.addEventListener('load', ()=> {
			this.vimeoVideos.forEach((video) => {
				this._createVimeo(video);
			});
			this.eventListeners('vimeo');
		})
	}

	/*
	*	 Initialize Youtube video
	*/
	_createYT(video) {
		return new YT.Player(video.elementId, {
			height: '100%',
			width: '100%',
			videoId: video.id,
			playerVars: {
				showinfo: 0,
				rel: 0,
			},
			parent: video.element,
			events: {
				onStateChange: this.onPlayerStateChange
			},
			color: 'white',
			modestbranding: 0
		});
	}

	/*
	*	 Initialize Vimeo video
	*/
	_createVimeo(video) {
		let options = {
			id: video.id,
			color: '00b2a9',
			width: '100%',
			height: '100%',
			transparent: 0
		};

		video.player = new Vimeo.Player(video.elementId, options);
	}

	/*
	* 	Set event listeners on video elements
	*/
	eventListeners(type) {
		if(type === 'youtube') {
			this.ytVideos.forEach((video) => {
				let iframe = video.element.querySelector('.m-video__iframe');
				video.poster.addEventListener('click', () => {
					iframe.classList.add('m-video__iframe--active');
					video.player.playVideo();
				})
			});
		} else if (type === 'vimeo') {
			this.vimeoVideos.forEach((video) => {
				let iframe = video.element.querySelector('.m-video__iframe');

				video.poster.addEventListener('click', () => {
					iframe.classList.add('m-video__iframe--active');
					video.player.play();
				});

				video.player.on('ended', function(data) {
					iframe.classList.remove('m-video__iframe--active');
				});
			});
		}

	}

	/*
	* Event when YouTube video is changing state.
	* to track if video is ended.
	*/
	onPlayerStateChange(event) {
		if(event.data === 0) {
			event.target.a.classList.remove('m-video__iframe--active');
		}
	}

}
module.exports = video;
