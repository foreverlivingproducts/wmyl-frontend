class selectedReseller {
	constructor() {
		this.resellers = document.querySelectorAll('.js-select-reseller');
		this.resellers = [].slice.call(this.resellers);
		this.elementWrapper = document.querySelector('.m-reseller-result');
		let elementWrapper = document.querySelector('.m-reseller-result');
		let resultList = document.querySelector('.o-reseller__result-list');
		this.signupWrapper = document.querySelector('.m-signup__reseller');
		this.signupTarget = document.querySelector('#distributor_id');

		let target = "";
		let button = "";

		if (resultList) {
			button = resultList.querySelector('.js-toggle-resellers');
			target = resultList.querySelector('.o-reseller__result-resellers');
			this.toggleResults(button, target);
		}

		if (elementWrapper) {
			this._toggleContactForm(elementWrapper, resultList, target, button);
			this._closeReseller(elementWrapper);
		}

		if (this.resellers.length > 0) {
			this.selectReseller(elementWrapper);
		}

		if (this.signupWrapper && this.signupTarget) {
			this.pickReseller();
		}
	}

	/* Toggle contact form */
	_toggleContactForm(elementWrapper, resultList, target, button) {
		let showFormBtn = elementWrapper.querySelector('.js-show-form');
		let hideFormBtn = elementWrapper.querySelector('.js-hide-form');
		let form = elementWrapper.querySelector('.m-reseller-result__form');
		let info = elementWrapper.querySelector('.m-reseller-result__person');

		if (!showFormBtn || !hideFormBtn) {
			return
		}

		/* Show form */
		showFormBtn.addEventListener('click', () => {
			info.classList.remove('js-is-visible');
			form.classList.add('js-is-visible');

			if (target) {
				target.classList.remove('js-is-visible');
				button.classList.remove('js-is-active');
				resultList.classList.add('js-is-hidden');
			}
		});

		/* Hide form */
		hideFormBtn.addEventListener('click', () => {
			form.classList.remove('js-is-visible');
			info.classList.add('js-is-visible');

			if (target) {
				target.classList.add('js-is-visible');
				button.classList.add('js-is-active');
				resultList.classList.remove('js-is-hidden');
			}
		});
	}

	/* Close selected reseller information */
	_closeReseller(elementWrapper) {
		let button = elementWrapper.querySelector('.js-close-reseller');

		button.addEventListener('click', () => {
			elementWrapper.classList.remove('js-is-visible');
		});
	}

	/* Open selected reseller information */
	selectReseller(elementWrapper) {
		this.resellers.forEach((element) => {
			this.openResellerElement(element, elementWrapper);
		});
	}

	openResellerElement(select, target) {
		select.addEventListener('click', () => {
			if (!target) {
				return
			}

			if (select.checked) {
				target.classList.add('js-is-visible');
			}
		});
	}

	/* Toggle visibility for result list */
	toggleResults(button, target) {
		button.addEventListener('click', () => {
			if (!target) {
				return
			}

			/* Open targeted elements */
			if (!button.classList.contains('js-is-active')) {
				target.classList.add('js-is-visible');
				button.classList.add('js-is-active');
			} else {
				target.classList.remove('js-is-visible');
				button.classList.remove('js-is-active');
			}
		});
	}

	pickReseller() {
		this.signupTarget.addEventListener('click', () => {
			const wrapper = this.elementWrapper.querySelector('.m-reseller-result__content');
			const mavItem = this.elementWrapper.querySelector('.m-reseller-result__link');
			const mavLink = mavItem ? mavItem.href : "#";
			const signupContent = this.signupWrapper.querySelector('.m-reseller-result__content');
			const id = this.signupWrapper.querySelector('#reseller-id');
			const signupLink = this.signupWrapper.querySelector('.m-reseller-result__actions-mav');
			id.value = this.signupTarget.value;
			signupContent.innerHTML = wrapper.innerHTML;
			signupLink.href = mavLink;
		})
	}
}

module.exports = selectedReseller;
