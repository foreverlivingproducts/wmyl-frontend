require('es6-promise').polyfill();
import 'whatwg-fetch';

class maps {
	constructor() {
		this.mapAtoms = document.querySelectorAll('.m-map');
		this.mapAtoms = [].slice.call(this.mapAtoms);
		this.markers = [];
		this.filterMarkers = [];
		this.visibleMaxCount = 80; // when more resellers in viewport list won't be populated
		this.randomize = true;

		if (this.mapAtoms.length >= 1) {
			this._setGoogleMaps();
		}
	}

	_setGoogleMaps() {
		this._loadGoogleMapsScript(() => {
			google.maps.event.addDomListener(window, 'load', init);
		});
	}

	/*
	*	Add YouTube Api as a script tag in header.
	*/
	_loadGoogleMapsScript(scriptLoaded) {
		const _this = this;
		if (typeof google !== 'undefined') scriptLoaded();

		let tag = document.createElement('script');

		tag.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDbgoO9E8e_0wZccMvlbkEWoHvigHAeKSU&libraries=places";
		let firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		tag.addEventListener('load', () => {
			_this.mapAtoms.forEach((map) => {
				let reseller = map.classList.contains('m-map--reseller');
				_this.init(map, reseller);
			});
		})
	}

	init(mapElement, reseller) {
		this.staticPath = mapElement.getAttribute('data-staticPath');

		let lat;
		let long;
		let zoom;

		if (reseller) {
			this.resellersPath = mapElement.getAttribute('data-resellerPath');

			const MarkerClusterer = require('js-marker-clusterer');
			this.visibleResellers = [];
			this.resellerWrapper = document.querySelector('.o-reseller__result');
			if(this.resellerWrapper) {
				this.resultList   = this.resellerWrapper.querySelector('.o-reseller__result-resellers');
				this.searchAmount = this.resellerWrapper.querySelector('.o-reseller__result-count');
				this.searchFilter = this.resellerWrapper.querySelector('.o-reseller__result-keyword');
				this.resultActive = this.resellerWrapper.querySelector('.o-reseller__result-selected');
				this.activeResellerBlock = {
					id		: this.resultActive.querySelector("#distributor_id"),
					name 	: this.resultActive.querySelector('.m-reseller-result__name'),
					cName	: this.resultActive.querySelector('.m-reseller-result__name span'),
					avatar 	: this.resultActive.querySelector(".m-reseller-result__image"),
					desc 	: this.resultActive.querySelector(".m-reseller-result__desc"),
					link 	: this.resultActive.querySelector(".m-reseller-result__link"),
					address : this.resultActive.querySelector(".m-reseller-result__details"),
				};

				this.avatarFallback = this.activeResellerBlock.avatar.innerHTML;
			}

			/* Center Scandinavia as first view */
			lat = '61.693054';
			long = '15.490274';
			zoom = 5;

		} else {
			lat = mapElement.getAttribute('data-map-lat');
			long = mapElement.getAttribute('data-map-long');
			zoom = 12;
		}

		const styles = [
			{
				"featureType": "administrative.province",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 20
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 30
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "road.local",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 40
					},
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
					{
						"hue": "#ffc859"
					},
					{
						"lightness": -25
					},
					{
						"saturation": -97
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "labels",
				"stylers": [
					{
						"visibility": "on"
					},
					{
						"lightness": -25
					},
					{
						"saturation": -100
					}
				]
			}
		];

		const mapOptions = {
			zoom: zoom,
			center: new google.maps.LatLng(lat, long),
			styles: styles,
		};

		if (reseller) {
			this.map = new google.maps.Map(mapElement, mapOptions);
			this.getResellers();
			this.resellersEvent();
			this.getLocationFromUrl();
		} else {
			 const map = new google.maps.Map(mapElement, mapOptions);
			 const marker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, long),
				map: map,
				title: 'Forever Aloe Vera',
				icon: this.staticPath+'/flp-pin.png'
			});
		}
	}

	getLocationFromUrl() {
		let input = document.getElementById('search-location');

		/* Get query */
		let request = {
			 QueryString : item => {
				let svalue = location.search.match(new RegExp("[\?\&]" + item + "=([^\&]*)(\&?)","i"));
				return svalue ? svalue[1] : svalue;
			}
		};

		let searchLocation = request.QueryString('location');
		let geo = request.QueryString('geo');

		/* Clean GET variables */
		let cleanSearch = decodeURIComponent(searchLocation);
		let cleanGeo = decodeURIComponent(geo);

		cleanSearch = cleanSearch.replace("+", " ");
		cleanGeo = JSON.parse(cleanGeo);

		/* Query actions */
		if(cleanGeo) {
			if(cleanSearch) {
				input.value = cleanSearch;
			}

			if(this.map){
				this.map.setCenter(cleanGeo.location);
				this.map.fitBounds(cleanGeo.viewport);
			}
		} else if (cleanSearch === 'geoLocation') {
			let geoLocation = document.querySelector('#geolocation');
			geoLocation.click();
		}

	}


	resellersEvent() {
		const _this = this;
		this.searchPlace = {};
		this.earlierSearch = '';

		let geoLocation = document.querySelector('#geolocation');
		let searchReseller = document.getElementById('search-person');
		let searchBtn = document.getElementById('searchReseller');
		let searchLocation = document.getElementById('search-location');

		const options = {
			componentRestrictions: {
				country: ["se","dk","no","fi" ]
			}
		};

		if (searchLocation) {
			/* Init Google search */
			let searchBox = new google.maps.places.Autocomplete(searchLocation, options);
			/* Set EventListener on change */
			searchBox.addListener('place_changed', function() {
				let place = searchBox.getPlace();
				if (place.length == 0) {
					return;
				}

				if (place.geometry.location) {
					_this.searchPlace = place.geometry;
				}
			});

			searchLocation.addEventListener('keyup', (e) => {
				if(e.keyCode === 13) {
					setTimeout(() => { this._triggerSearch(searchReseller, _this, searchLocation)}, 100);
				}
			})

		}

		if(searchReseller) {
			searchReseller.addEventListener('keyup', (e) => {
				if(e.keyCode === 13) {
					this._triggerSearch(searchReseller, _this, searchLocation);
				}
			})
		}

		if(geoLocation) {
			geoLocation.addEventListener('click', (e)  => {
				if(searchReseller.value) {
					_this.filterName(searchReseller.value);
				} else {
					this.filterMarkers = this.markers;
					this.reloadMarkers(this.filterMarkers);
				}
				_this.searchPlace.formatted_address = 'Nära mig';
				_this.getLocation();
			});

			geoLocation.addEventListener('keyup', (e) => {
				if(e.keyCode === 13) {
					this._triggerSearch(searchReseller, _this, searchLocation);
				}
			})
		}

		if(searchBtn) {
			searchBtn.addEventListener('click', (e)  => {
				this._triggerSearch(searchReseller, _this, searchLocation);
			});
			searchBtn.addEventListener('keyup', (e)  => {
				if(e.keyCode === 13) {
					this._triggerSearch(searchReseller, _this, searchLocation);
				}
			});
		}

		google.maps.event.addListener(this.map, 'idle', function() {
			_this.getVisibleResellers();
		});

		google.maps.event.addListener(this.map, 'bounds_changed', function() {
			_this.searchPlace.location = null;
		})
	}


	_triggerSearch(searchReseller, _this, searchLocation) {
		if (searchReseller.value) {
			_this.map.setZoom(4); // zoom out when searching name
			if (this.earlierSearch !== searchReseller.value) {
				this.earlierSearch = searchReseller.value;
				this.filterName(searchReseller.value);
			}
		} else {
			if (this.earlierSearch !== '') {
				this.earlierSearch = searchReseller.value;
				this.filterMarkers = this.markers;
				this.reloadMarkers(this.filterMarkers);
			}
		}

		if (_this.searchPlace.location) {
			_this.map.setCenter(_this.searchPlace.location);
			_this.map.fitBounds(_this.searchPlace.viewport);
		} else {
			_this.getVisibleResellers();
		}
		_this.searchPlace.formatted_address = '';

		//searchLocation.value = '';
	}

	resultListEvent() {
		let resellers = this.resultList.querySelectorAll('.o-reseller__result-item');
		resellers = [].slice.call(resellers);
		resellers.forEach(reseller => {
			reseller.addEventListener('click', () => {
				 let key = reseller.getAttribute('data-key');
				 key = parseInt(key);
				 this.populateActiveReseller(this.visibleResellers[key]);
			});
		})
	};

	getResellers() {
		const _this = this;
		if(this.resellersPath) {
			let myRequest = new Request(this.resellersPath);
			fetch(myRequest)
				.then(response => response.json())
				.then(data => {
					let sortData = data.sort(() => 0.5 - Math.random());
					_this.markResellers(sortData);
				})
				.catch(function(ex) {
					console.log('parsing failed', ex)
				});
		}else {
			console.log('Sorry! did not find Resellers');
		}
	}

	markResellers(resellers) {
		this.markers = [];
		let _this = this;

		resellers.forEach((reseller) => {
			let latLng = new google.maps.LatLng(reseller.latitude, reseller.longitude);
			let marker = new google.maps.Marker({
				position: latLng,
				map: this.map,
				icon: this.staticPath + '/flp-pin.png',
				reseller: reseller
			});

			marker.addListener('click', function () {
				_this.map.setCenter(marker.getPosition());
				_this.populateActiveReseller(marker.reseller);
			});

			this.markers.push(marker);
		});

		this.filterMarkers = this.markers;
		this.markerCluster = new MarkerClusterer(this.map, this.markers, {imagePath: this.staticPath+'/flp-m'});
		this.getVisibleResellers();
	}

	getLocation() {
		const _this = this;

		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				let pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				_this.map.setCenter(pos);
				_this.map.setZoom(13);
			});
		} else {
			alert('sorry your browser does not support Geolocation');
		}
	}

	getVisibleResellers() {
		this.visibleResellers = [];
		let inMapVisibleResellers = [];
		let resultWrapper = this.resellerWrapper.querySelector('.o-reseller__result-wrapper');

		for (let i = 0; i < this.filterMarkers.length; i++){
			let visible = this.inViewPort(this.map.getBounds(), this.filterMarkers[i].reseller);
			if (visible) {
				inMapVisibleResellers.push(this.filterMarkers[i].reseller);
			}
		}

		if(inMapVisibleResellers.length < this.visibleMaxCount) {
			if (this.randomize) {
				this.visibleResellers = this.shuffle(inMapVisibleResellers);
			} else {
				this.visibleResellers = inMapVisibleResellers;
				this.randomize = true;
			}
			this.resellerWrapper.querySelector('.o-reseller__result-resellers');
			resultWrapper.classList.remove('o-reseller__result-wrapper--hide');
		}
		else {
			// hide resellers list box
			resultWrapper.classList.add('o-reseller__result-wrapper--hide');
		}

		this.populateResellerList(this.visibleResellers);
	}

    inViewPort(latLngBounds, marker) {
        return latLngBounds.contains({lat: Number(marker.latitude), lng: Number(marker.longitude)});
    }

	filterName(name) {
		let filterMarkers = [];

 		name = name.toLowerCase();

		for (let i = 0; i < this.markers.length; i++){
			let combinedName = '';
			if(this.markers[i].reseller.firstname1) {
				combinedName = this.markers[i].reseller.firstname1.trim();
			}
			if(this.markers[i].reseller.surname1) {
				combinedName = combinedName + ' ' + this.markers[i].reseller.surname1.trim();
			}
			if(this.markers[i].reseller.firstname2) {
				combinedName = combinedName + ' ' + this.markers[i].reseller.firstname2.trim();
			}
			if(this.markers[i].reseller.surname2) {
				combinedName = combinedName + ' ' + this.markers[i].reseller.surname2.trim();
			}

			if (combinedName.toLowerCase().indexOf(name) >= 0) {
				filterMarkers.push(this.markers[i]);
			}
		}

		this.reloadMarkers(filterMarkers);
		this.filterMarkers = filterMarkers;
	}

	reloadMarkers(newMarkers) {
		if(this.markerCluster) {
			this.markerCluster.clearMarkers();
		}
		this.markerCluster = new MarkerClusterer(this.map, newMarkers, {imagePath: this.staticPath+'/flp-m'});
	}

	populateResellerList(markers) {
		if(this.resellerWrapper){
			this.searchAmount.innerHTML = markers.length;
			let filter = '';
			let list = '';
			let name = '';
			let result = document.querySelector('.o-reseller__result');

			if(markers.length == 0) {
				result.classList.add('o-reseller__result--no-result');
			}
			else {
				result.classList.remove('o-reseller__result--no-result');
			}


			if(this.earlierSearch && this.searchPlace.formatted_address) {
				filter = this.earlierSearch + ", " + this.searchPlace.formatted_address+ ": ";
			} else if(this.searchPlace.formatted_address) {
				filter = this.searchPlace.formatted_address+ ": ";
			} else if(this.earlierSearch){
				filter = this.earlierSearch+ ": ";
			}else {
				filter = '';
			}

			this.searchFilter.innerHTML = filter;

			markers.forEach((marker, key) => {
				(marker.firstname1 && marker.surname2)? name = marker.firstname1 + " " + marker.surname1 + ", " + marker.firstname2 +" "+ marker.surname2 : name = marker.firstname1+" "+ marker.surname1;
				list = list + "<li class='o-reseller__result-item' data-key="+ key +">" +
					"<input class=\"m-radio-custom m-radio-custom--align-top js-select-reseller\" id=" + marker.id + " data-reseller-id="+ marker.id +" type=\"radio\" name=\"reseller\" value=\"\">" +
					"<label class=\"a-label a-label--block\" for="+ marker.id +">"+ name +"</label>" +
					"<p class=\"o-reseller__result-address\">"+ marker.street_address + " - " + marker.zip_code + " " + marker.city + "</p>" +
					"</li>";
			});

			this.resultList.innerHTML = list;

			// Check if reseller information is open
			// If open, set active reseller as checked in list.
			let hasActive = this.resultActive.classList.contains('js-is-visible');
			if(hasActive) {
				let id = this.activeResellerBlock.id.value;
				let active = this.resellerWrapper.querySelector('[data-reseller-id="'+id+'"]');

				if (active) {
					active.setAttribute('checked', true);
				}
			}

			this.resultListEvent();
		}
	}

	populateActiveReseller(reseller) {
		if(this.resellerWrapper){
			// Populate Wrapper with the active Reseller
			let name = "";
			(reseller.firstname1 && reseller.surname2)? name = reseller.firstname1 + " " + reseller.surname1 + ", " + reseller.firstname2 +" "+ reseller.surname2 : name = reseller.firstname1+" "+ reseller.surname1;

			if (reseller.description === null || reseller.description === '') {
				reseller.description = '';
			}else {
				reseller.description = "<p>"+reseller.description+"</p>";
			}
			if (reseller.telephones === null) {
				reseller.telephones = '';
			}

			this.activeResellerBlock.id.value = reseller.id;
			if (reseller.avatar) {
				this.activeResellerBlock.avatar.innerHTML = "<img src='http://myaloevera.se"+ reseller.avatar +"' alt='avatar'/>"
			}
			else {
				this.activeResellerBlock.avatar.innerHTML = this.avatarFallback;
			}

			this.activeResellerBlock.name.innerHTML = name;

			if (this.activeResellerBlock.cName) {
				this.activeResellerBlock.cName.innerHTML = name;
			}

			if (this.activeResellerBlock.desc) {
				this.activeResellerBlock.desc.innerHTML = reseller.description;
			}

			this.activeResellerBlock.address.innerHTML = "<li class=\"o-reseller-result__detail\">"+reseller.street_address+"</li>" +
				"<li class=\"o-reseller-result__detail\">"+ reseller.zip_code + ", "+ reseller.city +"</li>" +
				"<li class=\"o-reseller-result__detail\">"+ reseller.telephones +"</li>";

			if (reseller.domain) {
				this.activeResellerBlock.link.classList.remove('m-reseller-result__link--hide');
				this.activeResellerBlock.link.href = reseller.domain;
			} else {
				this.activeResellerBlock.link.classList.add('m-reseller-result__link--hide');
				this.activeResellerBlock.link.href = '';
			}

			// Show reseller on map without shuffling
			this.randomize = false;
			let latLng = new google.maps.LatLng(reseller.latitude, reseller.longitude);
			this.map.setCenter(latLng);

			let zoom = this.map.getZoom();
			if(zoom < 14) {
				this.map.setZoom(14);
			}

			// Make active Reseller visible.
			this.resultActive.classList.add('js-is-visible');
		}
	}

	shuffle(array) {
		let currentIndex = array.length;
		let temporaryValue;
		let randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

}

module.exports = maps;
