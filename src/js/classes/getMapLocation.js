class getLocation {
	constructor() {
		this.locationForm = document.querySelector('.m-reseller-search');

		if (this.locationForm) {
			this.geoBtn = this.locationForm.querySelector('#geo-location');
			this.searchBtn = this.locationForm.querySelector('#send-location');
			this.input = this.locationForm.querySelector('#get-location');

			if (this.input) {
				this._setGoogleMaps();
			} else {
				this.addEventListeners();
			}
		}
	}

	_setGoogleMaps() {
		this._loadGoogleMapsScript(() => {
			google.maps.event.addDomListener(window, 'load', init);
		});
	}

	/*
	 * Add Google Api as a script tag in header.
	 */
	_loadGoogleMapsScript(onScriptLoaded) {
		if (typeof google !== 'undefined') onScriptLoaded();
		let tag = document.createElement('script');

		tag.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBRnfenJ-OytBLaPxxm7kA3ZWBJoOvbbxo&libraries=places";
		let firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		tag.addEventListener('load', () => this.init());
	}

	/*
	 * Init autocomplete
	 */
	init() {
		const options = {
			componentRestrictions: {
				country: ["se", "dk", "no", "fi"]
			}
		};
		/*set auto complete*/
		this.autocomplete = new google.maps.places.Autocomplete(this.input, options);

		this.addEventListeners();
	}

	addEventListeners() {
		const action = this.locationForm.getAttribute('action');
		let enterPressed = false;
		let hash = '';

		if (this.autocomplete) {
			this.autocomplete.addListener('place_changed', () => {
				let place = this.autocomplete.getPlace();
				if (place.length === 0) {
					return;
				}

				let geo = JSON.stringify(place.geometry);

				if (place.formatted_address && geo) {
					hash = 'location=' + place.formatted_address + '&geo=' + geo;
				} else {
					hash = '';
				}

				if (enterPressed) {
					window.location.href = action + "?" + hash;
				}
			});
		}

		if (this.input) {
			this.input.addEventListener('keydown', (e) => {
				if (e.keyCode === 13) {
					enterPressed = true;
					e.stopPropagation();
				}
			});
		}

		if (this.locationForm) {
			this.locationForm.addEventListener('submit', e => e.preventDefault());
		}

		if (this.searchBtn) {
			this.searchBtn.addEventListener('click', (e) => {
				e.preventDefault();
				if (hash) {
					window.location.href = action + "?" + hash;
				}
			});
		}

		if (this.geoBtn) {
			this.geoBtn.addEventListener('click', () => window.location.href = action + "?" + 'location=geoLocation')
		}
	}
}

module.exports = getLocation;
