class filterSearchResult {

	constructor() {
		this.filterBtns = document.querySelectorAll('.js-filter-result');
		this.filterBtns = [].slice.call(this.filterBtns);
		this.allTargets = document.querySelectorAll('[data-filter]');
		this.allTargets = [].slice.call(this.allTargets );

		if(this.filterBtns.length >= 1) {
			this.getElements();
		}
	}

	/* Get matching elements on the page */
	getElements() {
		this.filterBtns.forEach((element) => {
			let button = element;
			let type = element.getAttribute('data-filter-type');
			let target = document.querySelectorAll('[data-filter=' + type +']');
			this.filterElements(button, type, target);
		});
	}

	/* Show matching results */
	filterElements(button, type, target) {
		button.addEventListener('click', ()=> {
			let showAll = false;

			if (type === "filter-all") {
				showAll = true;
			}

			this.allTargets.forEach((element) => {
				let elementType =  element.getAttribute('data-filter');

				if (showAll) {
					element.classList.remove('js-is-hidden');
				} else {
					if (elementType !== type) {
						element.classList.add('js-is-hidden');
					} else {
						element.classList.remove('js-is-hidden');
					}
				}
			});

			if (!button.classList.contains('js-is-active') ) {
				// Remove active class from each button
				this.filterBtns.forEach((element) => {
					element.classList.remove('js-is-active');
				});

				// Add active class on current button
				button.classList.add('js-is-active');
			}
		});
	}
}
module.exports = filterSearchResult;
