const Instafeed = require('instafeed.js');
class instagram {
	constructor() {
		let instagramWrapper = document.querySelector('#instafeed');

		if(instagramWrapper) {
			this.setInstagram();
		}
	}

	setInstagram () {
		let feed = new Instafeed({
			clientId: "db055e30da8f49e2b9d65f262bd4b2a1",
			get: "user",
			userId: "415367041",
			accessToken: "415367041.db055e3.55eb33ea21ac4e7783caf12ff4fd3370",
			resolution: "standard_resolution",
			filter: function(image) {
				var MAX_LENGTH = 140;
				if (image.caption && image.caption.text) {
					if(image.caption.text.length > 200) {
						let cutCaption = image.caption.text.slice(0, MAX_LENGTH);
						let lastSpacing = cutCaption.lastIndexOf(' ');
						cutCaption = cutCaption.slice(0, lastSpacing);

						image.short_caption = cutCaption + '...';
					}else {
						image.short_caption = image.caption.text;
					}
				} else {
					image.short_caption = '';
				}
				return true;
			},
			template: '<a href="{{link}}" target="_blank" class="o-instagram__feed-item u-object-fit"><div class="o-instagram__image u-object-fit__container"><img class="u-object-fit__image" src="{{image}}"/></div><div class="o-instagram__caption">{{model.short_caption}}</div></div></a>',
			sortBy: 'most-recent',
			limit: 4,
			links: false
		});
		feed.run();
	}
}

module.exports = instagram;
