class toggleContent {
	constructor() {
		this.targetSelector = document.querySelectorAll('.js-toggle-content');
		this.targetSelector = [].slice.call(this.targetSelector);

		if (this.targetSelector.length >= 1) {
			this.getElements(this.targetSelector);
		}
	}

	/* Get matching elements on the page */
	getElements(targets, type) {
		targets.forEach((element) => {
			let button = element;
			let targetSelector = null;

			targetSelector = element.getAttribute('aria-controls');
			let target = document.getElementById(targetSelector);
			this.toggleElements(button, target);
		});
	}

	/* Toggle visibility on targeted elements */
	toggleElements(button, target) {
		button.addEventListener('click', ()=> {
			if (!target) {
				return
			}

			target.classList.toggle('js-is-visible');
			button.classList.toggle('js-is-active');
		});
	}
}

module.exports = toggleContent;
