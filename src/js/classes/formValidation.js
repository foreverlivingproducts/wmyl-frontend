class formValidation {

	constructor() {
		let forms = document.querySelectorAll('.js-form-validation');
		forms = [].slice.call(forms);
		if (forms.length >= 1) {
			forms.forEach((form) => {
				let fields = form.querySelectorAll('input[required="required"]');
				fields = [].slice.call(fields);
				let summary = document.querySelector('.m-signup__summary');

				if (fields.length >= 1) {
					this.requiredFormCheck(form, fields, summary);
				}
			})
		}
	}

	/* Check if required input fields contains something */
	requiredFormCheck(form, fields, summary) {
		if (!form) {
			return;
		}
		let emailCheck = form.querySelector('#emailInput');
		let confirmCheckbox = form.querySelector('.js-confirm');
		let button = form.querySelector('.js-validate-form');
		let emailValue = null;
		let isValid = false;

		if (button) {
			form.addEventListener('keyup', () => {
				if (emailCheck) {
					emailValue = emailCheck.value;
				}

				isValid = true;

				fields.forEach((element) => {
					if (element.value === '') {
						isValid = false;
						return false;
					}

					if (emailValue) {
						/* Check if e-mail addresses match */
						if (element.id === "emailInputConfirm") {
							if (element.value !== emailValue) {
								element.nextElementSibling.innerHTML = 'E-postadresserna matchar inte';
								isValid = false;
								return false;
							} else {
								element.nextElementSibling.innerHTML = '';
							}
						}
					}
				});

				/* Enable button if no field is empty */
				if (confirmCheckbox) {
					if (!confirmCheckbox.checked) {
						this.validateBtn(button, false, form, summary);
					} else {
						this.validateBtn(button, isValid, form, summary);
					}
				} else {
					this.validateBtn(button, isValid, form, summary);
				}
			});
		}

		if (confirmCheckbox) {
			this.validateBtn(button, false);

			confirmCheckbox.addEventListener('change', () => {
				let confirm = true;

				if (!confirmCheckbox.checked) {
					confirm = false;
				} else {
					fields.forEach((element) => {
						if (element.value === '') {
							confirm = false;
							return false;
						}

						if (emailValue) {
							/* Check if e-mail addresses match */
							if (element.id === "emailInputConfirm") {
								if (element.value !== emailValue) {
									element.nextElementSibling.innerHTML = 'E-postadresserna matchar inte';
									confirm = false;
									return false;
								} else {
									element.nextElementSibling.innerHTML = '';
								}
							}
						}
					});
				}
				this.validateBtn(button, confirm, form, false);
			});
		}
	}

	/* Enable button if no field is empty */
	validateBtn(button, validated, form, summary) {
		if (validated === true) {
			button.removeAttribute("disabled");

			if (summary) {
				this.formSummary(form, summary);
			}
		} else {
			button.setAttribute("disabled", true);
		}
	}

	/* Display all values for confirmation  */
	formSummary(form, parent) {
		this.allFields = form.querySelectorAll('input, select');
		this.allFields = [].slice.call(this.allFields);
		let output = '';

		this.allFields.forEach((element) => {
			let value = element.value;
			let label = element.previousElementSibling;

			if (label) {
				label = label.innerHTML;
				label = label.replace(' *', '');
			}

			if (value !== '') {
				if (element.tagName === "SELECT") {
					/* Only enabled options */
					if (!element.options[element.selectedIndex].disabled) {
						output += '<div class="m-signup__summary-item"><span class="m-signup__summary-label">' + label + ': </span>' + '<span class="m-signup__summary-value">' + value + '</span></div>';
					}
				} else if (element.id !== "emailInputConfirm") {
					output += '<div class="m-signup__summary-item"><span class="m-signup__summary-label">' + label + ': </span>' + '<span class="m-signup__summary-value">' + value + '</span></div>';
				}
			}
		});
		parent.innerHTML = output;
	}
}

module.exports = formValidation;
