import Swiper from 'swiper/dist/js/swiper.js';

class imageSlider {
	constructor() {
		this.imageSliders = document.querySelectorAll('.swiper-container');

		if(this.imageSliders) {
			this.initSlider();
		}
	}

	initSlider (){
		new Swiper ('.o-image-slider', {
			direction: 'horizontal',
			loop: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			spaceBetween: 60,
			clickable: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				770: {
					spaceBetween: 40,
				}
			}
		});
	}


}

module.exports = imageSlider;
