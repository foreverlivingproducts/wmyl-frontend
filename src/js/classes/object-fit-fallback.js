class objectFit {
	constructor() {
		if(!Modernizr.objectfit) {
			this.images = document.querySelectorAll('.u-object-fit');
			this.images = [].slice.call(this.images);

			if(this.images) {
				this.getElements();
			}
		}
	}

	/* Get matching elements on the page */
	getElements() {
		this.images.forEach((element) => {
			let container = element.querySelector('.u-object-fit__container');
			let imageSrc = container.querySelector('.u-object-fit__image').src;
			this.setBackground(container, imageSrc);
		});
	}

	/* Set image src as background style on parent */
	setBackground(container, imageSrc) {
		if (container) {
			container.style.backgroundImage = "url('" + imageSrc +  "')";
		}
	}
}

module.exports = objectFit;
