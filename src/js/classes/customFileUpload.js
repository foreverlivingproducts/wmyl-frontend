class customFileUpload {

	constructor() {
		this.inputs = document.querySelectorAll('.m-file-input');

		if(this.inputs.length >= 1) {
			this.getElements();
		}
	}

	/* Get matching elements on the page */
	getElements() {
		this.inputs.forEach((element) => {
			this.setFileName(element);
		});
	}

	/* Set selected file name as label text */
	setFileName(input) {
		let label = input.nextElementSibling;
		let labelText = label.innerHTML;

		input.addEventListener('change', function(e) {
			let fileName = '';

			if (this.files) {
				fileName = e.target.value.split('\\').pop();
			}

			if (fileName) {
				label.innerHTML = fileName;
			} else {
				label.innerHTML = labelText;
			}
		});

		// Firefox bug fix
		input.addEventListener('focus', function(){
			input.classList.add('has-focus');
		});

		input.addEventListener('blur', function(){
			input.classList.remove('has-focus');
		});
	}
}
module.exports = customFileUpload;
