class toggleContent {
	constructor() {
		this.targetSelector = document.querySelectorAll('.js-toggle-menu');
		this.targetSelector = [].slice.call(this.targetSelector);

		if (this.targetSelector.length >= 1) {
			this.getElements();
		}
	}

	/* Get matching elements on the page */
	getElements() {
		this.targetSelector.forEach((element) => {
			let button = element;
			let target = button.nextElementSibling;
			this.toggleElements(button, target);
		});
	}

	/* Toggle visibility on targeted elements */
	toggleElements(button, target) {
		button.addEventListener('click', ()=> {
			if (!button) {
				return
			}

			if( !button.classList.contains('js-is-active') ) {
				target.classList.add('js-is-visible');
				button.classList.add('js-is-active');
			} else {
				target.classList.remove('js-is-visible');
				button.classList.remove('js-is-active');
			}
		});
	}
}

module.exports = toggleContent;
