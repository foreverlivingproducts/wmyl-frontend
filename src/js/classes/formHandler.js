require('es6-promise').polyfill();
import 'whatwg-fetch';

class formHandler {

	constructor() {
		this.forms = document.querySelectorAll('form');
		this.forms = [].slice.call(this.forms);

		if(this.forms.length >= 1) {
			this.init();
		}
	}

	init() {
		this.forms.forEach((form) => {
			let simpleAction = form.classList.contains('js-simple-action');
			if(!simpleAction) {
				let action = form.getAttribute('action');
				if(action){
					this.submitForm(form, action);
				}
			}

		})
	}

	submitForm(form, action) {
		let validationWrapper = form.querySelector('.js-form-response');

		form.addEventListener('submit', (e) => {
            //console.log('submit', form);
            e.preventDefault();
            let values = {};
            let inputs = form.querySelectorAll('input, textarea');
            let data = new FormData();
            let message = "";

            inputs.forEach((input) => {
                // files can not be part of the json data
                if(input.getAttribute('type') == 'file') {
                    data.append(input.getAttribute('name'), input.files[0]);
                }
                else {
                    let name = input.getAttribute('name');
                    let value = input.value;
                    values[name] = value;
                }
            });
            //console.log(values);
            data.append( "json", JSON.stringify( values ) );

			fetch(action,
				{
					method: "POST",
					body: data
				})
				.then(function(res){ return res.json(); })
				.then(function(data){
					//console.log(data);
					if(data['success']) {
						message = "<p class='a-feedback'>Tack vi har nu skickat ditt mail!</p>";
						form.reset();
					}else {
						message = "<p class='a-feedback a-feedback--error'>Oj, något gick fel. Se över det du skrivit och försök gärna igen.</p>";
					}

					if(validationWrapper) {
						validationWrapper.classList.remove('js-is-hidden');
						validationWrapper.innerHTML = message;
					}
				})
				.catch(function(ex) {
					//console.log('parsing failed', ex);
					message = "<p class='a-feedback a-feedback--error'>Oj, något gick fel. Se över det du skrivit och försök gärna igen.</p>";

					if(validationWrapper) {
						validationWrapper.classList.remove('js-is-hidden');
						validationWrapper.innerHTML = message;
					}
				}
			);
		})
	}

}

module.exports = formHandler;
