class maps {
	constructor() {
		this.buttons = document.querySelectorAll('.js-go-to-target');
		this.buttons = [].slice.call(this.buttons);

		if (this.buttons.length >= 1) {
			this.getElements();
		}
	}

	/*
	 * Get matching elements on the page
	 */
	getElements() {
		this.buttons.forEach((element) => {
			let button = element;
			let targetSelector = element.getAttribute('data-target');
			let target = document.getElementById(targetSelector);
			this.goToTarget(button, target);
		});
	}

	/*
	 * Scroll to target when pressing the button
	 */
	goToTarget(button, target) {
		button.addEventListener('click', ()=> {
			if (!target) {
				return
			}

			// Open target
			if (!target.classList.contains('.js-is-visible')) {
				target.classList.add('js-is-visible');
			}

			target.scrollIntoView(
				{
					behavior: "smooth",
					block: "start"
				}
			);
		});
	}
}

module.exports = maps;
