const svg = require('./includes/svg4everybody/svg4everybody');
require('./includes/modernizr-custom');
const customFileUpload = require('./classes/customFileUpload');
const filterSearchResult = require('./classes/filterSearchResult');
const formValidation = require('./classes/formValidation');
const formHandler = require('./classes/formHandler');
const goToTarget = require('./classes/goToTarget');
const getLocation = require('./classes/getMapLocation');
const video = require('./classes/video');
const instagram = require('./classes/instagram');
const objectFit = require('./classes/object-fit-fallback');
const selectedReseller = require('./classes/selectedReseller');
const toggleContent = require('./classes/toggleContent');
const toggleHeaderContent = require('./classes/toggleHeaderContent');
const toggleSubmenu = require('./classes/toggleSubmenu');
const imageSlider = require('./classes/imageSlider');
const maps = require('./classes/maps');

class App {
	constructor() {
		svg();
		new customFileUpload();
		new filterSearchResult();
		new formValidation();
		new formHandler();
		new goToTarget();
		new getLocation();
		new selectedReseller();
		new toggleContent();
		new toggleHeaderContent();
		new toggleSubmenu();
		new video();
		new instagram();
		new objectFit();
		new imageSlider();
		new maps();
	}
}

new App();
