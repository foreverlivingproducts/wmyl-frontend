# Font Assets

If you are providing web font files, this is the place to put them.

If you don't plan using web fonts, or are relying on an external service like Google Fonts, feel free to delete this folder and the associated config.

## Tasks and Files
