# SCSS

Settings: Global variables and site wide settings

Tools: Global functions and mixins

Generic: Normalize. Low-specificity, far-reaching rulesets

Utilities: Helper classes and overrides

## Atomic design
Build your components from an Atomic design point of view.

Atoms: Often unclassed, abstract HTML elements, not to useful on their own, but still functions as the foundation of the project

Molecules:  Combined group of atoms with their own properites, built for reuse.

Organisms: Combined group of molecules, builds relatively complex sections of the interface.

Read more about Atomic design: http://bradfrost.com/blog/post/atomic-web-design/

## BEM
Use BEM as methodology to structure your components and making them reusable and scalable.

Read more about BEM: http://getbem.com/introduction/

## Guidelines
Follow our WMYLs guidelines when writing your CSS or read more about general guidelines here: http://codeguide.co/
